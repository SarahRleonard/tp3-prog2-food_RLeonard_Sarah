<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = Food::with('users')->orderBy('created_at', 'desc')->paginate(9);//va paginer tous les 4 articles
        return view('food',compact('foods')); 
        //on met les infos qui vienne de la bd dans une variable qui s'appelle $post
      //$posts = Post::all();//all dis d'aller chercher toute les infos de la bd Post
      //$posts = Post::paginate(10); //va faire 10 pages
      //$posts = Post::orderBy('created_at', 'desc')->paginate(4);//va paginer tous les 4 articles
      //return view('welcome',compact('posts'));
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('add_food');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = "";
        
        if ($request->image) {
        $image = basename($request->image->store('images', 'public')); /* on lui dit d'enregistrer l'image dans le dossier public, celui  à la racine */

        /* Ne fonctionne pas avec MAMP */
       /*  $image = InterventionImage::make($request->image)->widen(500)->encode();
            Storage::put('public/thumbs/' . $path, $image); */ 
        }

        $food = new Food();
        $food->description = $request->description;
        $food->image = $request->image;
        $food->meteo= $request->meteo;
        $food->user_id= $request->user_id;
        $food->is_reserved= $request->is_reserved;
        $food->created_at= $request->created_at;

        $food->image = $image; //pour l'image

        $food->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $food = Food::find($id);
        //dd($food);
        //dd($food->categories);
        return View('onefood', compact('food')); //affiche un seul met/food avec l'id - compact c'est la variable
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food = Food::find($id);
        return View('edit_food', compact('food'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $food = Food::find($id);

        $image = $food -> image;
        
        if($request->image){
            $image = basename($request->image->store('images', 'public'));
             /* Ne fonctionne pas avec MAMP */
       /*  $image = InterventionImage::make($request->image)->widen(500)->encode();
            Storage::put('public/thumbs/' . $path, $image); */ 
        }

        $food->description = $request->description;
        $food->image = $image;
        $food->meteo = $request->meteo;
        $food->user_id = $request->user_id;
        $food->is_reserved = $request->is_reserved;
        $food->created_at = $request->created_at;

        $food->save();

        return redirect('/dashboard');
    }
    /**
     * Reserve a meal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*  public function reserve(Request $request, $id)
    {
        $food = Food::find($id);

        $food->user_id = $request->user_id;
        
        $food->is_reserved = $request->is_reserved;
        
        $food->is_reserved=1;
        
        $user_id->save();

        return redirect('/dashboard');
    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Food::destroy($id);
        if ($user) {
            return response()->json([
                'message' =>'Meal deleted successfully'
            ], 200);
            
        } else {
            return response()->json([
                'message' => 'did not worked, try again'
            ], 404);
        }
       return redirect('/dashboard');
    }
}
