<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserCOntroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $user->address = $request->address;
        $user->city = $request->city;
        $user->save();

        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = User::find(Auth::user()->id);
        $foods = Food::where('user_id',Auth::user()->id)->get();
        //dd($food);
        return View('dashboard', compact('user','foods'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::find(Auth::user()->id); //va chercher l'info dans la bd
        return view('edit_user', compact('user'));

        $request->validate([
            'name' => 'required|max:255|unique:users',
            'email' => 'required',
            'address' => 'required'
        ]);
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email; 
        $user->address = $request->address; 
        $user->city = $request->city;
        
        $user->save();

         /* $user->update(request()->all());  */
     
    return redirect('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        if ($user) {
            return response()->json([
                'message' =>'Account deleted successfully'
            ], 200);
            
        } else {
            return response()->json([
                'message' => 'did not worked, try again'
            ], 404);
        }
    }
}
