<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $table = 'foods';
    
    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class); //?? un repas peut voir un user_id 
    }
    public function users()
    {
        return $this->hasMany(User::class); //un user peut avoir plusieurs repas à offrir
    }
}
