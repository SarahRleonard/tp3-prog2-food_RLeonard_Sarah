<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */ 

Route::get('/', [FoodController::class, 'index'])->name('food');


Route::get('/dashboard',[UserController::class,'show'])->middleware(['auth'])->name('dashboard');

Route::get('/edit-user/{id}',[UserController::class,'edit'])->middleware(['auth'])->name('edit_user');
Route::post('/edit-user/{id}',[UserController::class,'update'])->middleware(['auth'])->name('edit_user');

Route::delete('/user/{id}', [UserController::class,'destroy'])->middleware(['auth'])->name('delete_user'); //quand ça va appeler cette route, avec delete, il va appeler la methode destroy
/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard'); */


Route::get('/onefood/{id}',[FoodController::class,'show'])->name('onefood'); //show permet de voir un article
Route::get('/reservefood/{id}',[FoodController::class,'reserve'])->name('reservefood');

Route::get('/add_food', [FoodController::class,'create'])->middleware('auth')->name('add_food');//créer le met
Route::post('/add_food', [FoodController::class,'store']);//enregistre le met

Route::get('/edit-food/{id}', [FoodController::class,'edit'])->middleware(['auth'])->name('edit_food');//créer le met
Route::post('/edit-food/{id}', [FoodController::class,'update'])->middleware(['auth'])->name('edit_food');//enregistre le met

Route::delete('/food/{id}', [FoodController::class,'destroy'])->middleware(['auth'])->name('delete_food'); //quand ça va appeler cette route, avec delete, il va appeler la methode destroy

//Route::post('/food/{id}',[FoodController::class,'store']); À modifier

require __DIR__.'/auth.php';
