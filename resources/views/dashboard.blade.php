<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>


    {{-- copier dans {{ $slot }} dans le app.blade --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @auth
                    <div class="p-6 bg-white border-b border-gray-200">
                        Welcome in your profil {{ $user->name }}!
                    </div>
                    <div class="p-6 bg-white border-b border-gray-200">

                        {{-- @foreach ($food->food_id as $food_id) --}}
                        {{-- ne devrais pas s'afficher si je ne suis pas login --}}
                        Name: {{ $user->name }}
                        <br>
                        Email: {{ $user->email }}
                        <br>
                        Address: {{ $user->address }}
                        <br>
                        City: {{ $user->city }}
                        <br>
                        @include('inc.edit_user')
                        @include('inc.delete_user')
                        <br>

                        Meal Reserved
                        <br>

                        <br>
                        @if ($user->food_id)
                            @if ($user->food->image)
                                <img src="/storage/images/{{ $user->food->image }}" alt="mon image">
                            @endif
                            Description: {{ $user->food->description }}
                            <br>
                            Date posted: {{ $user->food->created_at->format('j M Y, G:i ') }}
                            <br>
                            Meteo : {{ $user->food->meteo }}
                            {{-- @include('inc.pickup_food') --}}
                        @else

                            No meal reserved

                        @endif

                        <br>
                        <br>

                        <br>

                        <br>
                        <h3>Meals offered<h3>
                            
                                @forelse ($foods as $food)
                                
                                        <br>
                                        @if ($food->image)
                                            <img src="/storage/images/{{ $food->image }}" alt="mon image">
                                        @endif
                                        Meal description: {{ $food->description }}
                                        <br>
                                        Meal address: {{ $food->address }}
                                        <br>
                                        Meal city: {{ $food->city }}
                                        <br>
                                        Outside weather: {{ $food->meteo }} degree
                                        <br>
                                        @if ($food->created_at)
                                            Date posted :{{ $food->created_at->format('j M Y, G:i ') }}
                                        @else
                                            Updated on : {{ $food->updated_at->format('j M Y, G:i ') }}
                                        @endif
                                        {{-- ne devrais pas s'afficher si je ne suis pas login --}}
                                        <br>
                                        @include('inc.edit_food')
                                        <br>
                                        @include('inc.delete_food')

                                        @empty
                                            <br>
                                            No meal offerred
                                            <br>
                                            <br>
                                            
                                    @endforelse
                                    
                                    <br>




                                @endauth

                    </div>


                </div>
            </div>
        </div>
    </x-app-layout>
