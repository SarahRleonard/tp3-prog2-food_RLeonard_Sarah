<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('email',255)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('address',255)->nullable();
            $table->string('city',255)->nullable();
            $table->unsignedBigInteger('food_id')->nullable();
            $table->string('password',255);
            $table->rememberToken();
            $table->timestamps();
            
            $table->foreign('food_id')
            ->references('id')
            ->on('foods')
            ->onDelete('cascade'); //null pour qu'il reste dans la base de donné
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
