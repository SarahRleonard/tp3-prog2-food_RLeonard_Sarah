<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::disableForeignKeyConstraints();
        /*Schema::table('foods', function(Blueprint $table){
            
            $table->string('image')->after('description');
        });*/
        Schema::enableForeignKeyConstraints(); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('foods', function(Blueprint $table){
            
            $table->dropColumn('image');
        });
        Schema::enableForeignKeyConstraints();
    }
}
